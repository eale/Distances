import re
import sys

import nltk
import pymorphy2
import unicodedata


def tokenize_text(text):
    tokenized_text = nltk.tokenize.word_tokenize(text, language="russian")
    return tokenized_text


def remove_stop_words(tokens):
    stop_words = nltk.corpus.stopwords.words("russian")
    codepoints = range(sys.maxunicode + 1)
    punctuation = {c for i in codepoints if unicodedata.category(c := chr(i)).startswith("P")}
    nums = re.compile(r"[+-]?\d+(?:\.\d+)?")
    filtered_tokens = []

    for token in tokens:
        if token not in stop_words and token not in punctuation and not nums.search(token):
            filtered_tokens.append(token)

    return filtered_tokens


def normalize_text(tokens):
    morph = pymorphy2.MorphAnalyzer()
    normalized_tokens = []

    for token in tokens:
        normalized_token = morph.parse(token)[0].normal_form
        normalized_tokens.append(normalized_token)

    return normalized_tokens


def get_dict_subjects():
    with open("subjects/news.txt", "r") as file:
        news = file.read().split("\n")

    with open("subjects/science.txt", "r") as file:
        science = file.read().split("\n")

    with open("subjects/shopping.txt", "r") as file:
        shopping = file.read().split("\n")

    with open("subjects/sport.txt", "r") as file:
        sport = file.read().split("\n")

    result = {"sport": sport, "shopping": shopping,
              "science": science, "news": news}

    return result
