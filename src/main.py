from src.work_with_text import normalize_text, remove_stop_words, tokenize_text, get_dict_subjects
from metrics import get_jaccard_distance, get_cosine_similarity


def main():
    with open("text.txt", "r") as file:
        text = file.read()

    normalized_text = normalize_text(remove_stop_words(tokenize_text(text)))
    subjects = get_dict_subjects()

    for key, subject in subjects.items():
        print(key)
        print(get_jaccard_distance(subject, normalized_text))
        print(get_cosine_similarity(subject, normalized_text))


if __name__ == '__main__':
    main()
