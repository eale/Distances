from п.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity


def get_jaccard_distance(text_1: list, text_2: list):
    text_set_1 = set(text_1)
    text_set_2 = set(text_2)

    shared = text_set_1.intersection(text_set_2)
    total = text_set_1.union(text_set_2)

    result = len(shared) / len(total)

    return result


def get_cosine_similarity(text_1: list, text_2: list):
    text_1 = ' '.join(text_1)
    text_2 = ' '.join(text_2)

    data = text_1, text_2

    count_vectorizer = CountVectorizer()
    vector_matrix = count_vectorizer.fit_transform(data)

    cosine_similarity_matrix = cosine_similarity(vector_matrix)
    return cosine_similarity_matrix[0][1]
